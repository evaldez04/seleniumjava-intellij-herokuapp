# Selenium Java using JUnit and Maven

This project is using JUnit framework in creating test automation in Selenium Java with the help of Maven

### Helpful Links

* https://www.oracle.com/java/technologies/javase-downloads.html --- download Java
* https://www.jetbrains.com/idea/download/ --- download IntelliJ
* http://maven.apache.org/download.cgi --- download Maven
* https://www.selenium.dev/maven/
* https://testng.org/doc/maven.html
* URL: http://the-internet.herokuapp.com/

## Pre-requisite

1. Download and install Java SDK
1. Update System Variable and include the following under PATH
     * Variable Name: JAVA_HOME     
     * Variable Value: Path of Java
1. Verify Java is installed successfully by running the command

```bash
java -version
```

## Install IntelliJ

* Download and install IntelliJ using the link provided
* You may use Community edition

## Install Maven

1. Download and install maven using the provided link
1. Add Maven path in System Variable
1. Create a new project in IntelliJ and use Maven as the project type
1. Update the POM.xml file to add the following dependencies: Selenium WebDriver

## How to run test