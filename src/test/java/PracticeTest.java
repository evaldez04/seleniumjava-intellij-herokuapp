import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class PracticeTest {
    static WebDriver driver;

    @BeforeClass
    public static void DriverInitiation() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\esteen\\testAutomation\\webDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Before
    public void NavigateToHerokuAppSite() {
        driver.get("http://the-internet.herokuapp.com/");
    }

    @Test
    public void Test1() {
        driver.findElement(By.linkText("Add/Remove Elements")).click();
        String currentUrl = driver.getCurrentUrl();
        Assert.assertEquals("http://the-internet.herokuapp.com/add_remove_elements/", currentUrl);
    }

    @Test
    public void DeleteButtonShouldBeDisplayed() {
        driver.findElement(By.linkText("Add/Remove Elements")).click();
        driver.findElement(By.xpath("//*[@onclick='addElement()']")).click();
        boolean deleteBtnDisplayed = driver.findElement(By.xpath("//*[@id='elements']/button[@class='added-manually']")).isDisplayed();
        Assert.assertTrue("Delete button is not displayed", deleteBtnDisplayed);
    }

    @AfterClass
    public static void CloseBrowser() {
        driver.quit();
    }
}
